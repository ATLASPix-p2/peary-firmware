//                              -*- Mode: Verilog -*-
// Filename        : FIFO_interface.sv
// Description     : Generic FIFO interface.
// Author          : Adrian Fiergolski
// Created On      : Mon Jul 10 11:15:24 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

interface FIFO_interface #(DATA_IN_SIZE = 8, DATA_OUT_SIZE = DATA_IN_SIZE, USED_WIDTH = 1);

   logic [DATA_IN_SIZE-1:0]  din;
   logic [DATA_OUT_SIZE-1:0] dout;
   logic                   din_en, dout_en;
   logic                   din_full;
   logic                   din_alfull;
   logic                   dout_empty;
   logic                   dout_valid;
   logic                   din_clear;     //synchronous clear
   logic [USED_WIDTH-1:0]  din_used;
   logic [USED_WIDTH-1:0]  dout_used;
                   
   wire                    wr_clk;
   wire                    rd_clk;
   wire                    rst;

endinterface // FIFO_interface

     
  
