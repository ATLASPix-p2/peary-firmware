//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_waveGeneratorControlInterface.sv
// Description     : Interface controlling CLICpix2_waveGenerator
// Author          : Adrian Fiergolski
// Created On      : Mon May  8 16:18:32 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

interface CLICpix2_waveGeneratorControlInterface #(NUMBER_OF_EVENTS=32) (input logic clk, input logic resetN);

   //Enable the sequence (synchronised with the AXI clk)
   logic en_axi;

   //Enable the sequence
   logic en;
   
   always_ff @(posedge clk) begin : SYNCHRONISE_EN
      (* ASYNC_REG = "TRUE" *) logic en_d[1:0];
      en <= en_d[1];
      en_d[1] <= en_d[0];
      en_d[0] <= en_axi;
   end

   //Loop the sequence (synchronised with the AXI clk)
   logic loop;

   //(synchronised with the AXI clk)
   typedef struct packed {
      logic 	  tp_sw;
      logic 	  pwr_pulse;      
      logic 	  shutter;
      logic [28:0] duration;} EVENT_T;

   //(synchronised with the AXI clk)
   EVENT_T events[32];
   
endinterface // CLICpix2_waveGeneratorControlInterface

