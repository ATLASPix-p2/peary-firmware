//                              -*- Mode: Verilog -*-
// Filename        : TLU_interface.sv
// Description     : The TLU interface.
// Author          : Adrian Fiergolski
// Created On      : Sat Jul  8 22:38:28 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

interface TLU_interface;
   logic clk;
   logic busy = 1'b0;
   logic trigger;
   logic reset;

   //TLU time
   logic [47:0] counter;
   
   always_ff @(posedge clk) begin : TLU_COUNTER
      if( reset == 1'b1)
	counter <= 0;
      else
	counter <= counter + 1;
   end
endinterface // TLU_interface

  
