//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_spi.sv
// Description     : Custom SPI block which generates SPI clock outside SPI transactions.
// Author          : Adrian Fiergolski
// Created On      : Tue Jul  4 18:49:24 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_spi_wrapper  #(AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 7, USE_CHIPSCOPE = 1)
  (
   //Fast clock used internally by the module
   input wire 				 ext_spi_clk,

   //Slow clock (2 times slower than ext_spi_clk) used as SPI clock
   //This clock is send to CLICpix2
   input wire 				 CLICpix2_slow_clock,
   
   //SPI
   output wire 				 CLICpix2_ss_p,
   output wire 				 CLICpix2_ss_n,
   output wire 				 CLICpix2_mosi_p,
   output wire 				 CLICpix2_mosi_n,
   input wire 				 CLICpix2_miso_p,
   input wire 				 CLICpix2_miso_n,
   //Clock is provided directly by the CaR clock generator
   //output wire 				 CLICpix2_slow_clock_p,
   //output wire 				 CLICpix2_slow_clock_n,
   
   ///////////////////////
   //AXI4-Lite interface
   ///////////////////////

    //Write address channel
   input wire [AXI_ADDR_WIDTH-1 : 0] 	 awaddr,
   input wire [2 : 0] 			 awprot,
   input wire 				 awvalid,
   output wire 				 awready,

    //Write data channel
   input wire [AXI_DATA_WIDTH-1 : 0] 	 wdata,
   input wire [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
   input wire 				 wvalid,
   output wire 				 wready,
   
    //Write response channel
   output wire [1 : 0] 			 bresp,
   output wire 				 bvalid,
   input wire 				 bready,

    //Read address channel
   input wire [AXI_ADDR_WIDTH-1 : 0] 	 araddr,
   input wire [2 : 0] 			 arprot,
   input wire 				 arvalid,
   output wire 				 arready,

    //Read data channel
   output wire [AXI_DATA_WIDTH-1 : 0] 	 rdata,
   output wire [1 : 0] 			 rresp,
   output wire 				 rvalid,
   input wire 				 rready,
   
   input wire 				 aclk,
   input wire 				 aresetN,
   output wire 				 ip2intc_irpt
   );

   CLICpix2_spi  #( .AXI_DATA_WIDTH(AXI_DATA_WIDTH), .AXI_ADDR_WIDTH(AXI_ADDR_WIDTH), .USE_CHIPSCOPE(USE_CHIPSCOPE) )
   spi(
       .ext_spi_clk(ext_spi_clk), .CLICpix2_slow_clock(CLICpix2_slow_clock),
       
       .CLICpix2_ss_p(CLICpix2_ss_p), .CLICpix2_ss_n(CLICpix2_ss_n),
       .CLICpix2_mosi_p(CLICpix2_mosi_p), .CLICpix2_mosi_n(CLICpix2_mosi_n),
       .CLICpix2_miso_p(CLICpix2_miso_p), .CLICpix2_miso_n(CLICpix2_miso_n),
//       .CLICpix2_slow_clock_p(CLICpix2_slow_clock_p), .CLICpix2_slow_clock_n(CLICpix2_slow_clock_n),

       //AXI4-Lite interface
      
       //Write address channel
       .awaddr(awaddr),
       .awprot(awprot),
       .awvalid(awvalid),
       .awready(awready),
      
       //Write data channel
       .wdata(wdata),
       .wstrb(wstrb),
       .wvalid(wvalid),
       .wready(wready),
      
       //Write response channel
       .bresp(bresp),
       .bvalid(bvalid),
       .bready(bready),
      
       //Read address channel
       .araddr(araddr),
       .arprot(arprot),
       .arvalid(arvalid),
       .arready(arready),
      
       //Read data channel
       .rdata(rdata),
       .rresp(rresp),
       .rvalid(rvalid),
       .rready(rready),
      
       .aclk(aclk),
       .aresetN(aresetN ),
       .ip2intc_irpt(ip2intc_irpt)
       );


endmodule // CLICpix2_spi_wrapper

