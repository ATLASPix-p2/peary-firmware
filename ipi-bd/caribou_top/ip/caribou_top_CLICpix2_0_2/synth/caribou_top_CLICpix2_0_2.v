// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:CLICpix2:1.0
// IP Revision: 2

(* X_CORE_INFO = "CLICpix2,Vivado 2017.3.1" *)
(* CHECK_LICENSE_TYPE = "caribou_top_CLICpix2_0_2,CLICpix2,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module caribou_top_CLICpix2_0_2 (
  AXI_araddr,
  AXI_arburst,
  AXI_arcache,
  AXI_arlen,
  AXI_arlock,
  AXI_arprot,
  AXI_arqos,
  AXI_arready,
  AXI_arregion,
  AXI_arsize,
  AXI_arvalid,
  AXI_awaddr,
  AXI_awburst,
  AXI_awcache,
  AXI_awlen,
  AXI_awlock,
  AXI_awprot,
  AXI_awqos,
  AXI_awready,
  AXI_awregion,
  AXI_awsize,
  AXI_awvalid,
  AXI_bready,
  AXI_bresp,
  AXI_bvalid,
  AXI_rdata,
  AXI_rlast,
  AXI_rready,
  AXI_rresp,
  AXI_rvalid,
  AXI_wdata,
  AXI_wlast,
  AXI_wready,
  AXI_wstrb,
  AXI_wvalid,
  C3PD_reset_n,
  C3PD_reset_p,
  CLICpix2_miso_n,
  CLICpix2_miso_p,
  CLICpix2_mosi_n,
  CLICpix2_mosi_p,
  CLICpix2_pwr_pulse_n,
  CLICpix2_pwr_pulse_p,
  CLICpix2_reset_n,
  CLICpix2_reset_p,
  CLICpix2_shutter_n,
  CLICpix2_shutter_p,
  CLICpix2_ss_n,
  CLICpix2_ss_p,
  CLICpix2_tp_sw_n,
  CLICpix2_tp_sw_p,
  SI5345_CLK_OUT8_clk_n,
  SI5345_CLK_OUT8_clk_p,
  TLU_busy_n,
  TLU_busy_p,
  TLU_reset_n,
  TLU_reset_p,
  TLU_trigger_n,
  TLU_trigger_p,
  Transceiver_RX_n,
  Transceiver_RX_p,
  Transceiver_refClk_clk_n,
  Transceiver_refClk_clk_p,
  aclk,
  aresetN,
  control_irpt,
  pll_locked,
  receiver_irpt,
  spi_irpt
);

(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARADDR" *)
input wire [30 : 0] AXI_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARBURST" *)
input wire [1 : 0] AXI_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARCACHE" *)
input wire [3 : 0] AXI_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARLEN" *)
input wire [7 : 0] AXI_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARLOCK" *)
input wire [0 : 0] AXI_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARPROT" *)
input wire [2 : 0] AXI_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARQOS" *)
input wire [3 : 0] AXI_arqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARREADY" *)
output wire AXI_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARREGION" *)
input wire [3 : 0] AXI_arregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARSIZE" *)
input wire [2 : 0] AXI_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI ARVALID" *)
input wire AXI_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWADDR" *)
input wire [30 : 0] AXI_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWBURST" *)
input wire [1 : 0] AXI_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWCACHE" *)
input wire [3 : 0] AXI_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWLEN" *)
input wire [7 : 0] AXI_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWLOCK" *)
input wire [0 : 0] AXI_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWPROT" *)
input wire [2 : 0] AXI_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWQOS" *)
input wire [3 : 0] AXI_awqos;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWREADY" *)
output wire AXI_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWREGION" *)
input wire [3 : 0] AXI_awregion;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWSIZE" *)
input wire [2 : 0] AXI_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI AWVALID" *)
input wire AXI_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BREADY" *)
input wire AXI_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BRESP" *)
output wire [1 : 0] AXI_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI BVALID" *)
output wire AXI_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RDATA" *)
output wire [31 : 0] AXI_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RLAST" *)
output wire AXI_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RREADY" *)
input wire AXI_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RRESP" *)
output wire [1 : 0] AXI_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI RVALID" *)
output wire AXI_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WDATA" *)
input wire [31 : 0] AXI_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WLAST" *)
input wire AXI_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WREADY" *)
output wire AXI_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WSTRB" *)
input wire [3 : 0] AXI_wstrb;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI, DATA_WIDTH 32, PROTOCOL AXI4, ID_WIDTH 0, ADDR_WIDTH 31, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN caribou_top_processing_system7_0_0_FCLK_CLK0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI WVALID" *)
input wire AXI_wvalid;
output wire C3PD_reset_n;
output wire C3PD_reset_p;
input wire CLICpix2_miso_n;
input wire CLICpix2_miso_p;
output wire CLICpix2_mosi_n;
output wire CLICpix2_mosi_p;
output wire CLICpix2_pwr_pulse_n;
output wire CLICpix2_pwr_pulse_p;
output wire CLICpix2_reset_n;
output wire CLICpix2_reset_p;
output wire CLICpix2_shutter_n;
output wire CLICpix2_shutter_p;
output wire CLICpix2_ss_n;
output wire CLICpix2_ss_p;
output wire CLICpix2_tp_sw_n;
output wire CLICpix2_tp_sw_p;
(* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 SI5345_CLK_OUT8 CLK_N" *)
input wire SI5345_CLK_OUT8_clk_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME SI5345_CLK_OUT8, FREQ_HZ 100000000, CAN_DEBUG false" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 SI5345_CLK_OUT8 CLK_P" *)
input wire SI5345_CLK_OUT8_clk_p;
output wire TLU_busy_n;
output wire TLU_busy_p;
input wire TLU_reset_n;
input wire TLU_reset_p;
input wire TLU_trigger_n;
input wire TLU_trigger_p;
input wire Transceiver_RX_n;
input wire Transceiver_RX_p;
(* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 Transceiver_refClk CLK_N" *)
input wire Transceiver_refClk_clk_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME Transceiver_refClk, FREQ_HZ 320000000, CAN_DEBUG false" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 Transceiver_refClk CLK_P" *)
input wire Transceiver_refClk_clk_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.ACLK, FREQ_HZ 200000000, PHASE 0.000, ASSOCIATED_BUSIF AXI, ASSOCIATED_RESET aresetN, CLK_DOMAIN caribou_top_processing_system7_0_0_FCLK_CLK0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.ACLK CLK" *)
input wire aclk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.ARESETN, POLARITY ACTIVE_LOW" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.ARESETN RST" *)
input wire aresetN;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME INTR.CONTROL_IRPT, SENSITIVITY EDGE_RISING, PortWidth 1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 INTR.CONTROL_IRPT INTERRUPT" *)
output wire control_irpt;
output wire pll_locked;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME INTR.RECEIVER_IRPT, SENSITIVITY EDGE_RISING, PortWidth 1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 INTR.RECEIVER_IRPT INTERRUPT" *)
output wire receiver_irpt;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME INTR.SPI_IRPT, SENSITIVITY EDGE_RISING, PortWidth 1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 INTR.SPI_IRPT INTERRUPT" *)
output wire spi_irpt;

  CLICpix2 inst (
    .AXI_araddr(AXI_araddr),
    .AXI_arburst(AXI_arburst),
    .AXI_arcache(AXI_arcache),
    .AXI_arlen(AXI_arlen),
    .AXI_arlock(AXI_arlock),
    .AXI_arprot(AXI_arprot),
    .AXI_arqos(AXI_arqos),
    .AXI_arready(AXI_arready),
    .AXI_arregion(AXI_arregion),
    .AXI_arsize(AXI_arsize),
    .AXI_arvalid(AXI_arvalid),
    .AXI_awaddr(AXI_awaddr),
    .AXI_awburst(AXI_awburst),
    .AXI_awcache(AXI_awcache),
    .AXI_awlen(AXI_awlen),
    .AXI_awlock(AXI_awlock),
    .AXI_awprot(AXI_awprot),
    .AXI_awqos(AXI_awqos),
    .AXI_awready(AXI_awready),
    .AXI_awregion(AXI_awregion),
    .AXI_awsize(AXI_awsize),
    .AXI_awvalid(AXI_awvalid),
    .AXI_bready(AXI_bready),
    .AXI_bresp(AXI_bresp),
    .AXI_bvalid(AXI_bvalid),
    .AXI_rdata(AXI_rdata),
    .AXI_rlast(AXI_rlast),
    .AXI_rready(AXI_rready),
    .AXI_rresp(AXI_rresp),
    .AXI_rvalid(AXI_rvalid),
    .AXI_wdata(AXI_wdata),
    .AXI_wlast(AXI_wlast),
    .AXI_wready(AXI_wready),
    .AXI_wstrb(AXI_wstrb),
    .AXI_wvalid(AXI_wvalid),
    .C3PD_reset_n(C3PD_reset_n),
    .C3PD_reset_p(C3PD_reset_p),
    .CLICpix2_miso_n(CLICpix2_miso_n),
    .CLICpix2_miso_p(CLICpix2_miso_p),
    .CLICpix2_mosi_n(CLICpix2_mosi_n),
    .CLICpix2_mosi_p(CLICpix2_mosi_p),
    .CLICpix2_pwr_pulse_n(CLICpix2_pwr_pulse_n),
    .CLICpix2_pwr_pulse_p(CLICpix2_pwr_pulse_p),
    .CLICpix2_reset_n(CLICpix2_reset_n),
    .CLICpix2_reset_p(CLICpix2_reset_p),
    .CLICpix2_shutter_n(CLICpix2_shutter_n),
    .CLICpix2_shutter_p(CLICpix2_shutter_p),
    .CLICpix2_ss_n(CLICpix2_ss_n),
    .CLICpix2_ss_p(CLICpix2_ss_p),
    .CLICpix2_tp_sw_n(CLICpix2_tp_sw_n),
    .CLICpix2_tp_sw_p(CLICpix2_tp_sw_p),
    .SI5345_CLK_OUT8_clk_n(SI5345_CLK_OUT8_clk_n),
    .SI5345_CLK_OUT8_clk_p(SI5345_CLK_OUT8_clk_p),
    .TLU_busy_n(TLU_busy_n),
    .TLU_busy_p(TLU_busy_p),
    .TLU_reset_n(TLU_reset_n),
    .TLU_reset_p(TLU_reset_p),
    .TLU_trigger_n(TLU_trigger_n),
    .TLU_trigger_p(TLU_trigger_p),
    .Transceiver_RX_n(Transceiver_RX_n),
    .Transceiver_RX_p(Transceiver_RX_p),
    .Transceiver_refClk_clk_n(Transceiver_refClk_clk_n),
    .Transceiver_refClk_clk_p(Transceiver_refClk_clk_p),
    .aclk(aclk),
    .aresetN(aresetN),
    .control_irpt(control_irpt),
    .pll_locked(pll_locked),
    .receiver_irpt(receiver_irpt),
    .spi_irpt(spi_irpt)
  );
endmodule
