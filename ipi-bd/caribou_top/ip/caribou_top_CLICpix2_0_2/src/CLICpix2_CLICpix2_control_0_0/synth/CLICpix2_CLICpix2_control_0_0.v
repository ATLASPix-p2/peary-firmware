// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:CLICpix2_control:1.0
// IP Revision: 5

(* X_CORE_INFO = "CLICpix2_control_wrapper,Vivado 2017.3.1" *)
(* CHECK_LICENSE_TYPE = "CLICpix2_CLICpix2_control_0_0,CLICpix2_control_wrapper,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module CLICpix2_CLICpix2_control_0_0 (
  CLICpix2_reset_p,
  CLICpix2_reset_n,
  CLICpix2_pwr_pulse_p,
  CLICpix2_pwr_pulse_n,
  CLICpix2_shutter_p,
  CLICpix2_shutter_n,
  CLICpix2_tp_sw_p,
  CLICpix2_tp_sw_n,
  CLICpix2_slow_clock,
  C3PD_reset_p,
  C3PD_reset_n,
  TLU_clk,
  TLU_trigger_p,
  TLU_trigger_n,
  TLU_busy_p,
  TLU_busy_n,
  TLU_reset_p,
  TLU_reset_n,
  ip2intc_irpt,
  awaddr,
  awprot,
  awvalid,
  awready,
  wdata,
  wstrb,
  wvalid,
  wready,
  bresp,
  bvalid,
  bready,
  araddr,
  arprot,
  arvalid,
  arready,
  rdata,
  rresp,
  rvalid,
  rready,
  aclk,
  aresetN
);

output wire CLICpix2_reset_p;
output wire CLICpix2_reset_n;
output wire CLICpix2_pwr_pulse_p;
output wire CLICpix2_pwr_pulse_n;
output wire CLICpix2_shutter_p;
output wire CLICpix2_shutter_n;
output wire CLICpix2_tp_sw_p;
output wire CLICpix2_tp_sw_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLICpix2_slow_clock, FREQ_HZ 100000000, PHASE 122.4, CLK_DOMAIN CLICpix2_ClockBuffer_0_0_Clk_out" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLICpix2_slow_clock CLK" *)
input wire CLICpix2_slow_clock;
output wire C3PD_reset_p;
output wire C3PD_reset_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME TLU_clk, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN CLICpix2_ClockBuffer_0_0_Clk_out" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 TLU_clk CLK" *)
input wire TLU_clk;
input wire TLU_trigger_p;
input wire TLU_trigger_n;
output wire TLU_busy_p;
output wire TLU_busy_n;
input wire TLU_reset_p;
input wire TLU_reset_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ip2intc_irpt, SENSITIVITY EDGE_RISING, PortWidth 1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 ip2intc_irpt INTERRUPT" *)
output wire ip2intc_irpt;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite AWADDR" *)
input wire [7 : 0] awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite AWPROT" *)
input wire [2 : 0] awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite AWVALID" *)
input wire awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite AWREADY" *)
output wire awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite WDATA" *)
input wire [31 : 0] wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite WSTRB" *)
input wire [3 : 0] wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite WVALID" *)
input wire wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite WREADY" *)
output wire wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite BRESP" *)
output wire [1 : 0] bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite BVALID" *)
output wire bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite BREADY" *)
input wire bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite ARADDR" *)
input wire [7 : 0] araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite ARPROT" *)
input wire [2 : 0] arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite ARVALID" *)
input wire arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite ARREADY" *)
output wire arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite RDATA" *)
output wire [31 : 0] rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite RRESP" *)
output wire [1 : 0] rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite RVALID" *)
output wire rvalid;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME axi-lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 200000000, ID_WIDTH 0, ADDR_WIDTH 8, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN CLICpix2_aclk, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi-lite RREADY" *)
input wire rready;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME aclk, ASSOCIATED_BUSIF axi-lite, ASSOCIATED_RESET aresetN, FREQ_HZ 200000000, PHASE 0.000, CLK_DOMAIN CLICpix2_aclk" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 aclk CLK" *)
input wire aclk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME aresetN, POLARITY ACTIVE_LOW" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 aresetN RST" *)
input wire aresetN;

  CLICpix2_control_wrapper #(
    .AXI_DATA_WIDTH(32),
    .AXI_ADDR_WIDTH(8),
    .WAVE_CONTROL_EVENTS_NUMBER(32),
    .USE_CHIPSCOPE(1)
  ) inst (
    .CLICpix2_reset_p(CLICpix2_reset_p),
    .CLICpix2_reset_n(CLICpix2_reset_n),
    .CLICpix2_pwr_pulse_p(CLICpix2_pwr_pulse_p),
    .CLICpix2_pwr_pulse_n(CLICpix2_pwr_pulse_n),
    .CLICpix2_shutter_p(CLICpix2_shutter_p),
    .CLICpix2_shutter_n(CLICpix2_shutter_n),
    .CLICpix2_tp_sw_p(CLICpix2_tp_sw_p),
    .CLICpix2_tp_sw_n(CLICpix2_tp_sw_n),
    .CLICpix2_slow_clock(CLICpix2_slow_clock),
    .C3PD_reset_p(C3PD_reset_p),
    .C3PD_reset_n(C3PD_reset_n),
    .TLU_clk(TLU_clk),
    .TLU_trigger_p(TLU_trigger_p),
    .TLU_trigger_n(TLU_trigger_n),
    .TLU_busy_p(TLU_busy_p),
    .TLU_busy_n(TLU_busy_n),
    .TLU_reset_p(TLU_reset_p),
    .TLU_reset_n(TLU_reset_n),
    .ip2intc_irpt(ip2intc_irpt),
    .awaddr(awaddr),
    .awprot(awprot),
    .awvalid(awvalid),
    .awready(awready),
    .wdata(wdata),
    .wstrb(wstrb),
    .wvalid(wvalid),
    .wready(wready),
    .bresp(bresp),
    .bvalid(bvalid),
    .bready(bready),
    .araddr(araddr),
    .arprot(arprot),
    .arvalid(arvalid),
    .arready(arready),
    .rdata(rdata),
    .rresp(rresp),
    .rvalid(rvalid),
    .rready(rready),
    .aclk(aclk),
    .aresetN(aresetN)
  );
endmodule
