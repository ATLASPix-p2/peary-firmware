-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
-- Date        : Thu Jan 11 12:17:50 2018
-- Host        : adrian-laptop running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode funcsim
--               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_xlconstant_0_1/caribou_top_xlconstant_0_1_sim_netlist.vhdl
-- Design      : caribou_top_xlconstant_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity caribou_top_xlconstant_0_1 is
  port (
    dout : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of caribou_top_xlconstant_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of caribou_top_xlconstant_0_1 : entity is "caribou_top_xlconstant_0_1,xlconstant_v1_1_3_xlconstant,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of caribou_top_xlconstant_0_1 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of caribou_top_xlconstant_0_1 : entity is "xlconstant_v1_1_3_xlconstant,Vivado 2017.3.1";
end caribou_top_xlconstant_0_1;

architecture STRUCTURE of caribou_top_xlconstant_0_1 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
begin
  dout(2) <= \<const1>\;
  dout(1) <= \<const0>\;
  dout(0) <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
end STRUCTURE;
