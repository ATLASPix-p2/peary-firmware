//                              -*- Mode: Verilog -*-
// Filename        : PLL_reset_wrapper.v
// Description     : The module resets PLL.
// Author          : Adrian Fiergolski
// Created On      : Wed Aug  9 19:01:03 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module PLL_reset_wrapper(
		 input wire  clk,
		 input wire  locked,
		 output wire reset);

   PLL_reset(.clk(clk),
	     .locked(locked),
	     .reset(reset) );
   
endmodule // PLL_reset_wrapper
