set_property PACKAGE_PIN P25 [get_ports C3PD_reset_p]
set_property PACKAGE_PIN P30 [get_ports CLICpix2_reset_p]
set_property PACKAGE_PIN T30 [get_ports CLICpix2_pwr_pulse_p]
set_property PACKAGE_PIN T29 [get_ports CLICpix2_shutter_p]
set_property PACKAGE_PIN R25 [get_ports CLICpix2_tp_sw_p]
set_property PACKAGE_PIN AF19 [get_ports CLICpix2_ss_p]
set_property PACKAGE_PIN AH19 [get_ports CLICpix2_mosi_p]

set_property PACKAGE_PIN AG24 [get_ports CLICpix2_miso_p]

set_property PACKAGE_PIN A17 [get_ports {led[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[0]}]
set_property PACKAGE_PIN W21 [get_ports {led[1]}]
set_property PACKAGE_PIN Y21 [get_ports {led[3]}]
set_property PACKAGE_PIN G2 [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[3]}]

##CLICpix2 SerDes

#Clock
set_property PACKAGE_PIN AD10 [get_ports Transceiver_refClk_clk_p]
set_property PACKAGE_PIN AD9 [get_ports Transceiver_refClk_clk_n]

#Rx data
set_property PACKAGE_PIN AH10 [get_ports Transceiver_RX_p]
set_property PACKAGE_PIN AH9 [get_ports Transceiver_RX_n]
#connect_debug_port dbg_hub/clk [get_pins -hier -filter {name=~*DRP_CLK_BUFG*O}]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

#TLU signals
#set_property PACKAGE_PIN AG21 [get_ports TLU_CLK_p]
#set_property PACKAGE_PIN AH21 [get_ports TLU_CLK_n]
set_property PACKAGE_PIN AH23 [get_ports TLU_TRG_p]
set_property PACKAGE_PIN AH24 [get_ports TLU_TRG_n]
set_property PACKAGE_PIN AD21 [get_ports TLU_BSY_p]
set_property PACKAGE_PIN AE21 [get_ports TLU_BSY_n]
set_property PACKAGE_PIN AA22 [get_ports TLU_RST_p]
set_property PACKAGE_PIN AA23 [get_ports TLU_RST_n]

#Clock signal from SI5345
set_property PACKAGE_PIN AE22 [get_ports SI5345_CLK_OUT8_clk_p]
set_property PACKAGE_PIN AF22 [get_ports SI5345_CLK_OUT8_clk_n]
set_property DIFF_TERM true [get_ports SI5345_CLK_OUT8_*]
set_property IOSTANDARD LVDS_25 [get_ports SI5345_CLK_OUT8_*]

set_property C_CLK_INPUT_FREQ_HZ 320000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets caribou_top_i/CLICpix2_0/inst/clk_wiz_0/transceiver_rx]
